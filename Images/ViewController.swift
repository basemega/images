//
//  ViewController.swift
//  Images
//
//  Created by Матвей Кравцов on 17.07.15.
//  Copyright (c) 2015 Матвей Кравцов. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var image: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func Generate(sender: AnyObject) {
        let url: NSURL = NSURL(string: "http://lorempixel.com/400/200/")!
        let request:NSMutableURLRequest = NSMutableURLRequest(URL:url)
                
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue()){
            (response: NSURLResponse?, data: NSData?, error: NSError?) in
            
            self.image.image = UIImage(data: data!)

        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

